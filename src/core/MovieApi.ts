const baseUrl = 'https://api.themoviedb.org/3/'
const apiKey = '5288a2a15c63d0eac94bc55cc1f7e6b3'

export default class MovieLists {
  async getUpcomingMovies() {
    const url = `${baseUrl}movie/upcoming?api_key=${apiKey}&language=en-US&page=1`
    const response = await fetch(url)
    const responseJson = await response.json()
    return responseJson
  }

  async getPopularMovies() {
    const url = `${baseUrl}trending/movie/week?api_key=${apiKey}&language=en-US&page=1`
    const response = await fetch(url)
    const responseJson = await response.json()
    return responseJson
  }

  async getTopRatedMovies() {
    const url = `${baseUrl}movie/top_rated?api_key=${apiKey}&language=en-US&page=1`
    const response = await fetch(url)
    const responseJson = await response.json()
    return responseJson
  }

  async searchMovie(title: string) {
    const url = `${baseUrl}search/movie?api_key=${apiKey}&language=en-US&page=1&query=${title}`
    const response = await fetch(url)
    const responseJson = await response.json()
    return responseJson
  }
}
