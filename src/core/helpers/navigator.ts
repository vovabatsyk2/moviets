import App from '../../pages/app'

export default class Navigator {
  private popularBtn: HTMLElement
  private upcomingBtn: HTMLElement
  private topRatedBtn: HTMLElement
  private searchBtn: HTMLElement
  private inpHtml: HTMLElement

  constructor() {
    this.popularBtn = <HTMLElement>document.querySelector('#popular')
    this.upcomingBtn = <HTMLElement>document.querySelector('#upcoming')
    this.topRatedBtn = <HTMLElement>document.querySelector('#top_rated')
    this.searchBtn = <HTMLElement>document.querySelector('#submit')
    this.inpHtml = <HTMLElement>document.querySelector('#search')
  }

  private listener() {
    this.popularBtn?.addEventListener('click', (e) => {
      window.location.hash = <string>this.popularBtn?.id
      e.preventDefault()
    })
    this.upcomingBtn.addEventListener('click', (e) => {
      window.location.hash = <string>this.upcomingBtn.id
      e.preventDefault()
    })
    this.topRatedBtn.addEventListener('click', (e) => {
      window.location.hash = <string>this.topRatedBtn.id
      e.preventDefault()
    })
    this.inpHtml.addEventListener('change', (e: any) => {
      const title = <string>e.target.value
      App.searchName = title
      e.target.value = ''
    })

    this.searchBtn.addEventListener('click', (e) => {
      e.preventDefault()
      window.location.hash = <string>this.searchBtn.id
    })
  }

  navigate() {
    this.listener()
  }
}
