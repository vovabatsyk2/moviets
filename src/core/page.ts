import MovieLists from './MovieApi'

abstract class Page {
  static TextObject = {}
  protected container: HTMLElement
  protected movies: MovieLists

  protected constructor() {
    this.container = <HTMLElement>document.createElement('div')
    this.container.className = 'row'
    this.movies = new MovieLists()
  }

  renderMovieHTML(image: string, text: string, date: string) {
    const cardWrapper = document.createElement('div')
    cardWrapper.className = 'col-lg-3 col-md-4 col-12 p-2'

    cardWrapper.innerHTML = `
    <div class="col-12 p-2">
        <div class="card shadow-sm">
            <img src="https://image.tmdb.org/t/p/original/${image}" />
            <svg xmlns="http://www.w3.org/2000/svg" stroke="red" fill="red" width="50" height="50"
                class="bi bi-heart-fill position-absolute p-2" viewBox="0 -2 18 22">
                <path fill-rule="evenodd"
                    d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z" />
            </svg>
            <div class="card-body">
                <p class="card-text truncate">${text}</p>
                <div class="d-flex justify-content-between align-items-center">
                    <small class="text-muted">${date}</small>
                </div>
            </div>
        </div>
    </div>`

    return cardWrapper
  }

  render(): HTMLElement {
    return this.container
  }
}

export default Page
