export default interface IMovie {
  backdrop_path: string
  overview: string
  release_date: string
}
