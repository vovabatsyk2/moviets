import IMovie from '../../core/interfaces/IMovie'
import Page from '../../core/page'

export default class Search extends Page {
  private searchText: string

  constructor(searchText: string) {
    super()

    this.searchText = searchText
  }

  render(): HTMLElement {
    this.movies.searchMovie(this.searchText).then((data) => {
      const movies = data.results
      movies.forEach((movie: IMovie) => {
        const movieHtml = this.renderMovieHTML(
          movie.backdrop_path,
          movie.overview,
          movie.release_date
        )
        this.container.append(movieHtml)
      })
    })
    return this.container
  }
}
