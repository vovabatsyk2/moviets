export const enum PageIds {
  PopularPage = 'popular',
  UpcomingPage = 'upcoming',
  TopRatedPage = 'top_rated',
  SearchingPage = 'submit',
}
