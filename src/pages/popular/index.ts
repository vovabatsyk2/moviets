import IMovie from '../../core/interfaces/IMovie'
import Page from '../../core/page'

export default class Popular extends Page {
  constructor() {
    super()
  }

  render(): HTMLElement {
    this.movies.getPopularMovies().then((data) => {
      const movies = data.results
      movies.forEach((movie: IMovie) => {
        const movieHtml = this.renderMovieHTML(
          movie.backdrop_path,
          movie.overview,
          movie.release_date
        )
        this.container.append(movieHtml)
      })
    })
    return this.container
  }
}
