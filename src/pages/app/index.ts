import Page from '../../core/page'
import { PageIds } from '../enums'
import Navigator from '../../core/helpers/navigator'
import TopRated from '../top-rated'
import Popular from '../popular'
import Upcoming from '../upcoming'
import Search from '../search'

export default class App {
  private static container: any = document.getElementById('film-container')
  private initialPage: Page
  private hasNavigator: Navigator
  public static searchName = 'Spider'

  static renderPage(idPage: string): void {
    App.container.innerHTML = ''
    let page: Page | null

    switch (idPage) {
      case PageIds.PopularPage:
        page = new Popular()
        if (page) {
          const pageHtml = page.render()
          App.container.append(pageHtml)
        }
        break
      case PageIds.UpcomingPage:
        page = new Upcoming()
        if (page) {
          const pageHtml = page.render()
          App.container.append(pageHtml)
        }
        break
      case PageIds.TopRatedPage:
        page = new TopRated()
        if (page) {
          const pageHtml = page.render()
          App.container.append(pageHtml)
        }
        break
      case PageIds.SearchingPage:
        page = new Search(App.searchName)
        if (page) {
          const pageHtml = page.render()
          App.container.append(pageHtml)
        }
        break
      default:
        page = new Popular()
        if (page) {
          const pageHtml = page.render()
          App.container.append(pageHtml)
        }
    }
  }
  constructor() {
    this.initialPage = new Popular()
    this.hasNavigator = new Navigator()
  }

  private enableRouteChange() {
    window.addEventListener('hashchange', () => {
      const hash: string = window.location.hash.slice(1)
      App.renderPage(hash)
    })
  }

  run(): void {
    this.hasNavigator.navigate()
    App.renderPage(PageIds.PopularPage)
    this.enableRouteChange()
  }
}
